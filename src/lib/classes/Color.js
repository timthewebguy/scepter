export default class Color {
  constructor (r = 0, g = 0, b = 0, a = 1) {
    this.r = r
    this.g = g
    this.b = b
    this.a = 1
  }

  toRGBA () {
    return `rgba(${this.r},${this.g},${this.b},${this.a})`
  }

  toRGB () {
    return `rgb(${this.r},${this.g},${this.b})`
  }

  toHEX () {
    let r = this.r.toString(16)
    r = r.length === 1 ? '0' + r : r

    let g = this.g.toString(16)
    g = g.length === 1 ? '0' + g : g

    let b = this.b.toString(16)
    b = b.length === 1 ? '0' + b : b

    return `#${r}${g}${b}`
  }

  static FromHex (hex, alpha = 1) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    let data = result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null

    if (data != null) {
      return new Color(data.r, data.g, data.b, alpha)
    } else {
      throw new Error('Unable to parse color hex code')
    }
  }

  static get White () {
    return new Color(255, 255, 255, 1)
  }

  static get Black () {
    return new Color(0, 0, 0, 1)
  }

  static get Transparent () {
    return new Color(128, 128, 128, 0)
  }
}
